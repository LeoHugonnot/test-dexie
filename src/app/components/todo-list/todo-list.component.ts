import { Component, ElementRef, OnInit } from '@angular/core';
import { db, TodoItem, TodoList } from 'src/app/services/db';
import { liveQuery } from "dexie";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  public listes$ = liveQuery(() => recupererListes());
  public _listeSelectionnee: TodoList | undefined;
  public todos$ = liveQuery(() => recupererTodos(this.listeSelectionnee?.idIndexedDB ?? 0));

  public nomListe: string = "";
  public nomTodo: string = "";

  constructor() { }

  ngOnInit(): void {

  }

  public ajouterListe() {
    db.todoLists.add({ title: this.nomListe });
    this.nomListe = "";
  }

  public ajouterTodo() {
    if (!this.listeSelectionnee?.idIndexedDB) return;
    db.todoItems.add({ title: this.nomTodo, todoListId: this.listeSelectionnee.idIndexedDB });
    this.nomTodo = "";
  }

  public mettreAJourTodo(todo: TodoItem) {
    if (!todo.idIndexedDB) return;
    db.todoItems.update(todo.idIndexedDB, todo);
  }

  public supprimerTodo(todo: TodoItem) {
    if (!todo.idIndexedDB) return;
    db.todoItems.delete(todo.idIndexedDB);
  }

  public supprimerListe() {
    if (!this.listeSelectionnee?.idIndexedDB) return;
    db.todoItems.where("todoListId").equals(this.listeSelectionnee.idIndexedDB).delete()
      .then(() => db.todoLists.delete(this.listeSelectionnee!.idIndexedDB!))
      .then(() => this.listeSelectionnee = undefined);
  }

  public get listeSelectionnee(): TodoList | undefined {
    return this._listeSelectionnee;
  }

  public set listeSelectionnee(liste: TodoList | undefined) {
    this._listeSelectionnee = liste;
    this.todos$ = liveQuery(() => recupererTodos(liste?.idIndexedDB ?? 0));
  }
}

async function recupererListes() {
  return await db.todoLists.toArray();
}

async function recupererTodos(listeId: number) {
  return await db.todoItems.where("todoListId").equals(listeId).toArray();
}