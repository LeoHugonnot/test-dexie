import Dexie, { Table } from "dexie";

export class TodoList {
    idIndexedDB?: number;

    constructor(public title: string) {

    }
}

export class TodoItem {
    idIndexedDB?: number;
    done?: boolean;

    constructor(
        public todoListId: number,
        public title: string,
    ) {

    }
}

export class AppDB extends Dexie {
    todoItems!: Table<TodoItem, number>;
    todoLists!: Table<TodoList, number>;

    constructor() {
        super("AppDB");
        this.version(1).stores({
            todoItems: "++idIndexedDB",
            todoLists: "++idIndexedDB, todoListId"
        });

        this.version(2).stores({
            todoItems: "++idIndexedDB, todoListId",
            todoLists: "++idIndexedDB"
        })
    }
}

export const db = new AppDB();